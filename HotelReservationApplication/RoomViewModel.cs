﻿namespace HotelReservationApplication
{
    public class RoomViewModel
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int NumberOfBeds { get; private set; }
    }
}
