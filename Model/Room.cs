﻿using Commons;
using Model;
using System.Collections.ObjectModel;

namespace HotelReservationApplication.Model
{
    public class Room
    {
        private readonly Lazy<List<Booking>> _currentBookings = new Lazy<List<Booking>>();
        public int Id { get; }
        public string Name { get; }
        public int NumberOfBeds { get; }
        public ReadOnlyCollection<Booking> CurrentBookings => new ReadOnlyCollection<Booking>(_currentBookings.Value);

        internal Room(int id, string name, int numberOfBeds)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            Id = id;
            Name = name;
            NumberOfBeds = numberOfBeds;
        }

        public void Book((DateTime StartDate, DateTime EndDate) bookRange)
        {
            foreach (var booking in _currentBookings.Value)
                if (DateTimeHelper.CheckColision(bookRange, booking.DateRange))
                    throw new Exception("Cannot book a room because of date collision");

            _currentBookings.Value.Add(new Booking(bookRange));
        }
    }
}
