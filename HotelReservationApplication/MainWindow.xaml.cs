﻿using HotelReservationApplication;
using JetBrains.Annotations;
using System;
using System.Windows;

namespace HotelReservation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow([NotNull] MainWindowViewModel viewModel)
        {
            if (viewModel is null)
                throw new ArgumentNullException(nameof(viewModel));

            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
