﻿using System;
using System.Collections.Generic;

namespace HotelReservationApplication.Services
{
    public interface IBookingService
    {
        IReadOnlyCollection<RoomViewModel> GetAll();
        IReadOnlyCollection<RoomViewModel> GetAvailable(DateTime? startDate, DateTime? endDate, int? numberOfBeds);
        void Book(int roomId, DateTime startDate, DateTime endDAte);
    }
}
