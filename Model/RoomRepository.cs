﻿using HotelReservationApplication.Model;

namespace Model
{
    public class RoomRepository : IRoomRepository
    {
        private readonly IReadOnlyCollection<Room> _fakeData;

        public RoomRepository()
        {
            _fakeData = DataInitializer.CreateInitialData();
        }

        public IReadOnlyCollection<Room> GetRooms() => _fakeData;
    }
}
