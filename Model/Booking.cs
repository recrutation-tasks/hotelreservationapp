﻿namespace HotelReservationApplication.Model
{
    public class Booking
    {
        internal Booking((DateTime StartDate, DateTime EndDate) dateRange)
        {
            DateRange = dateRange;
        }

        public (DateTime StartDate, DateTime EndDate) DateRange { get; }
    }
}
