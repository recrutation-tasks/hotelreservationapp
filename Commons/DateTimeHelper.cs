﻿
namespace Commons
{
    public static class DateTimeHelper
    {
        public static bool CheckColision(
            (DateTime? startDate, DateTime? endDate) first, 
            (DateTime? startDate, DateTime? endDate) second)
        {
            return first.startDate < second.endDate && second.startDate < first.endDate;
        }
    }
}
