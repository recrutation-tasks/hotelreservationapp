﻿using HotelReservationApplication.Model;

namespace Model
{
    public interface IRoomRepository
    {
        IReadOnlyCollection<Room> GetRooms();
    }
}
