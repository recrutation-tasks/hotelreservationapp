﻿using Ecommerce.Desktop.Client.Mapping;
using HotelReservationApplication.Model;
using JetBrains.Annotations;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HotelReservationApplication.Services
{
    public class BookingService : IBookingService
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IModelMapper _modelMapper;

        public BookingService([NotNull] IRoomRepository roomRepository, [NotNull] IModelMapper modelMapper)
        {
            _roomRepository = roomRepository ?? throw new ArgumentNullException(nameof(roomRepository));
            _modelMapper = modelMapper ?? throw new ArgumentNullException(nameof(modelMapper));
        }

        public void Book(int roomId, DateTime startDate, DateTime endDAte)
        {
            var room = _roomRepository.GetRooms().Single(x => x.Id == roomId);
            room.Book((startDate, endDAte));
        }

        public IReadOnlyCollection<RoomViewModel> GetAll()
            => _roomRepository
            .GetRooms()
            .Select(x => _modelMapper.Mapper.Map<Room, RoomViewModel>(x))
            .ToList();

        public IReadOnlyCollection<RoomViewModel> GetAvailable(DateTime? startDate, DateTime? endDate, int? numberOfBeds)
        {
            var filtered = _roomRepository
            .GetRooms()
            .Where(x => !numberOfBeds.HasValue || x.NumberOfBeds == numberOfBeds)
            .Where(x => !x.CurrentBookings.Any(cb => Commons.DateTimeHelper.CheckColision(cb.DateRange, (startDate, endDate)) )).ToList();

            return filtered.Select(x => _modelMapper.Mapper.Map<Room, RoomViewModel>(x))
            .ToList();
        }
            
    }
}
