﻿using AutoMapper;
using HotelReservationApplication;
using HotelReservationApplication.Model;

namespace Ecommerce.Desktop.Client.Mapping
{
    public class ModelMapper : IModelMapper
    {
        private readonly IMapper _mapper;

        public ModelMapper()
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Room, RoomViewModel>();
            }).CreateMapper();
        }

        public IMapper Mapper => _mapper;
    }
}
