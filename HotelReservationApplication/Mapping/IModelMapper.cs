﻿using AutoMapper;

namespace Ecommerce.Desktop.Client.Mapping
{
    public interface IModelMapper
    {
        IMapper Mapper { get; }
    }
}
