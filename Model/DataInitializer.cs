﻿using HotelReservationApplication.Model;

namespace Model
{
    internal static class DataInitializer
    {
        public static IReadOnlyCollection<Room> CreateInitialData()
        {
            Random rnd = new Random();
            var result = new List<Room>();
            for (int i = 1; i <= 10; i++)
                result.Add(new Room(i, "Room_" + i, rnd.Next(1, 4)));
            return result;
        }
    }
}
