﻿using HotelReservationApplication.Model;
using HotelReservationApplication.Services;
using JetBrains.Annotations;
using Microsoft.Xaml.Behaviors.Core;
using Model;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace HotelReservationApplication
{
    public class MainWindowViewModel : Prism.Mvvm.BindableBase
    {
        private readonly IBookingService _bookingService;
        private DateTime? _startDate;
        private DateTime? _endDate;
        private int _numberOfBeds;
        private RoomViewModel? _selectedRoom;

        public ICommand InitializeCommand { get; }
        public DelegateCommand BookCommand { get; }
        public ICommand SearchCommand { get; }
        public IReadOnlyCollection<RoomViewModel> FilteredRooms { get; private set; }
        public IReadOnlyCollection<int> AvailableRoomTypes { get; private set; }
        public DateTime? StartDate
        {
            get => _startDate;
            set {
                _startDate = value;
                BookCommand.RaiseCanExecuteChanged();
            } 
        }

        public DateTime? EndDate
        {
            get => _endDate;
            set {
                _endDate = value;
                BookCommand.RaiseCanExecuteChanged();
            } 
        }

        public int NumberOfBeds
        {
            get => _numberOfBeds;
            set => _numberOfBeds = value;
        }

        public RoomViewModel? SelectedRoom 
        {
            get => _selectedRoom;
            set {
                _selectedRoom = value;
                BookCommand.RaiseCanExecuteChanged();
            }
        }

        public MainWindowViewModel([NotNull] IBookingService bookingService)
        {
            _bookingService = bookingService ?? throw new ArgumentNullException(nameof(bookingService));
            InitializeCommand = new ActionCommand(() => Initialize());
            BookCommand = new DelegateCommand(BookARoom, CanBookARoom);
            SearchCommand = new ActionCommand(() => Search());
        }

        public void Initialize() 
        {
            AvailableRoomTypes = _bookingService
                .GetAll()
                .Select(x => x.NumberOfBeds)
                .Distinct()
                .ToList();
            OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(AvailableRoomTypes)));
            FilteredRooms = _bookingService.GetAvailable(null, null, null);
            OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(FilteredRooms)));
        } 

        public void BookARoom()
        {
            if (SelectedRoom != null)
                _bookingService.Book(SelectedRoom.Id, StartDate.Value, EndDate.Value);
            Search();
        }

        public bool CanBookARoom()
            => SelectedRoom != null && StartDate.HasValue && EndDate.HasValue;

        private void Search()
        {
            FilteredRooms = _bookingService.GetAvailable(_startDate, _endDate, _numberOfBeds);
            OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(FilteredRooms)));
            SelectedRoom = null;
            OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(SelectedRoom)));
        }
    }
}
